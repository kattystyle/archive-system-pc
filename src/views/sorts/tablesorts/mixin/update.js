import {
  getIdRulesDetail,
  getLastTableEdit,
  getSaveSettingsDetail,
  getSort,
  getStructDetail,
  getTableRelationDInfo, updateArNoPolicy
} from '@/api/system/sort'

export const update = {
  methods:{
    //  非底层修改按钮操作
    handleUpdate(row) {
      this.reset();
      const recNos = row ? row.recNo : this.ids
      if(this.queryParams.isLast === "1"){
        getSort(recNos).then(response => {
          this.form = response.data;
          if(response.data.sortSublist){
            this.form.sortSublist = response.data.sortSublist.split(",")
          }
          this.form.orderNo = response.data.orderNo.toString()
          this.open = true;
          this.title = "修改分类";
        });
      }else{
        this.handleLastTableUpdate(row)
      }
    },


    //开关按钮
    handleSwitch(row){
      console.log("==========开关按钮============");
    },

    // 底层表格修改按钮
    async handleLastTableUpdate(row){
      const recNos = row ? row.recNo : this.ids
      switch (this.nodeDataNo){
        case "001":
          this.tableInfoTitle = "修改表信息"
          const res = await getLastTableEdit(recNos)
          this.tableInfoForm = res.data
          this.tableInfoForm.orderNo = res.data.orderNo.toString()
          this.tableInfoOpen = true
          break
        case "002":
          this.tableRelationTitle = "修改表关系"
          const res2 = await getTableRelationDInfo(recNos)
          if(res2.code === 200){
            this.tableRelationForm = res2.data
            this.tableRelationForm.mainTableName = res2.data.parentNo
            this.tableRelationForm.childTableName = res2.data.childNo
            this.tableRelationForm.mainTableField = res2.data.arTableFiledRelation.parentFiled
            this.tableRelationForm.childTableField = res2.data.arTableFiledRelation.childFiled
            this.tableRelationForm.update = '修改'
            this.tableRelationDialogVisible = true
          }
          break
        case "003":
          this.dataStructureTitle = "修改数据结构"
          const res3 = await getStructDetail(recNos)
          if(res3.code === 200){
            this.dataStructureForm = res3.data
            this.dataStructureDialogVisible = true
          }
          break
        case "004":
          this.storeSettingsTitle = "修改存储设置"
          const res4 = await getSaveSettingsDetail(recNos)
          if(res4.code === 200){
            this.storeSettingsForm = res4.data
            this.storeSettingsDialogVisible = true
          }
          break
        case "005":
          this.NoRulesTitle = "修改档号规则"
          const res5 = await updateArNoPolicy(recNos)
          if(res5.code === 200){
            this.arNoPolicy = res5.data
            this.NoRulesDialogVisible = true
          }
          break
        case "006":
          this.IdRulesTitle = "修改鉴定规则"
          const res6 = await getIdRulesDetail(recNos)
          if(res6.code === 200){
            this.IdRulesForm = res6.data
            this.IdRulesForm.cate = this.queryParams.sortName
            // const arr = [res6.data.keepTimeBegin + ' '+ '00:00:00',res6.data.keepTimeEnd+ ' ' + '00:00:00']
            // this.$set( this.IdRulesForm, 'keepTime',arr)
            this.IdRulesDialogVisible = true
          }
          break
      }

    },
  }
}
