import {
  getArNoPolicyDict,
  getArNoPolicyList,
  getFConditions,
  getField,
  getFilterConditions,
  getIdRules,
  getSaveList, getSaveSettingsDict, getScopeList, getSecurityList,
  getStructList, getTableName,
  getTableRelationList,
  getTableRelationNameList, getTreeData,
  listSort,
  getTimeRange
} from '@/api/system/sort'

export const list = {
  methods:{
    // 获取左侧树形结构数据
    async getTree(){
      const res = await getTreeData()
      this.treeData = res.rows
      this.findLast(this.treeData)
    },
    // 查找底层数据
    findLast(arr){
      arr.forEach((item) => {
        item.last = '1'
        if(item.isLast==="0"){
          item.disabled = true
          item.last = '0'
          item.children = [{
            label: '表信息',
            recNo: item.recNo,
            sortName:item.sortName,
            sortMainTableNo:item.sortMainTableNo,
            disabled:true,
            rec: '001',
            isLast:'0'
          },{
            label: '表关系',
            recNo: item.recNo,
            sortName:item.sortName,
            sortMainTableNo:item.sortMainTableNo,
            disabled:true,
            rec:'002'
          },{
            label: '数据结构',
            recNo: item.recNo,
            sortName:item.sortName,
            sortMainTableNo:item.sortMainTableNo,
            disabled:true,
            rec:'003'
          },{
            label: '存储设置',
            recNo: item.recNo,
            sortName:item.sortName,
            sortMainTableNo:item.sortMainTableNo,
            disabled:true,
            rec:'004'
          },{
            label: '档号规则',
            recNo: item.recNo,
            sortMainTableNo:item.sortMainTableNo,
            sortName:item.sortName,
            disabled:true,
            rec:'005'
          },{
            label: '鉴定规则',
            recNo: item.recNo,
            sortName:item.sortName,
            sortMainTableNo:item.sortMainTableNo,
            disabled:true,
            rec:'006'
          }]
        }else if(item.children.length > 0){
          this.findLast(item.children)
        }
      })
    },
    // 获取弹窗表名称数据
    async getTableTitle(){
      const res = await getTableName()
      this.tableOptions = res.rows
    },
    // 查询分类管理列表
    getList() {
      this.loading = true;
      listSort(this.queryParams).then(response => {
        if(this.queryParams.isLast === "1"){
          this.sortList = response.rows;
          this.sortList.forEach((item,i) => {
            item.index = (this.queryParams.pageNum - 1) * this.queryParams.pageSize + i +1
          })
          this.orderNos = this.sortList.map(item => item.orderNo)
        }else{
          this.lastSortList = response.rows;
          this.lastSortList.forEach((item,i) => {
            item.index = (this.queryParams.pageNum - 1) * this.queryParams.pageSize + i +1
            item.tableType = item.tableType === "0" ? "目录表" : "元数据表"
          })
        }
        this.total = response.total;
        this.loading = false;
      });
    },
    // 获取表关系列表
    async getTableRelation(sortMainTableNo){
      this.lastTableLoading = true
      try {
        const res = await getTableRelationList(sortMainTableNo)
        this.lastSortList = res.rows
        this.lastSortList.forEach(item => {
          item.Filed = item.arTableFiledRelation.parentFiled + ' ' + '|'+ ' '+ item.arTableFiledRelation.childFiled
        })
        this.total = res.total;
      }catch (e) {
        this.$message.error(e)
      }finally {
        this.lastTableLoading = false
      }
    },
    // 获取表关系表名列表
    async gettableNameOptions(){
      const res = await getTableRelationNameList({isLast:0})
      this.TableNameOptions = res.rows
      this.TableNameOptions = this.TableNameOptions.filter(item => {
        return item.tableStatue === '1'
      })
    },
    // 获取结构信息列表
    async getStruct(recNo){
      this.lastTableLoading = true
      try {
        const res = await getStructList(recNo)
        this.lastSortList = res.rows
        this.total = res.total;
      }catch (e) {
        this.$message.error(e)
      }finally {
        this.lastTableLoading = false
      }

    },
    // 获取存储设置列表
    async getSave(recNo){
      this.lastTableLoading = true
      try {
        const res = await getSaveList(recNo);
        this.lastSortList = res.rows
        this.total = res.total;
      }catch (e) {
        this.$message.error(e)
      }finally {
        this.lastTableLoading = false
      }
    },
    // 获取存储设置下拉
    async getSaveSettingsOptions(data){
      const res = await getSaveSettingsDict({tableNo:data.sortMainTableNo})
      this.SaveSettingsOptions = res.rows
      this.SaveSettingsOptions.forEach(item => {
        item.dictLabel = item.fieldAlias
        item.dictValue = item.fieldName
      })
    },
    // 获取档号规则列表
    async getArNoPolicy(recNo){
      this.lastTableLoading = true
      try {
        const res = await getArNoPolicyList(recNo)
        this.lastSortList = res.rows
        this.lastSortList.forEach((item,i) => {
          item.isOpen = item.isOpen === "1" ? "启用" : "禁用"
        })
        this.total = res.total;
      }catch (e) {
        this.$message.error(e)
      }finally {
        this.lastTableLoading = false
      }
    },
    // 获取档号规则下拉
    async getArNoPolicyOptions(){
      const res = await getArNoPolicyDict()
      this.NoRulesOptions = res.data
    },
    // 获取鉴定规则列表
    async getIdRulesList(recNo){
      this.lastTableLoading = true
      this.addAvail = true;
      try {
        const res = await getIdRules(recNo)
        this.lastSortList = res.rows
        this.IdRuleList = res.rows
        this.lastSortList.forEach((item,i) => {
          item.isOpen = item.isOpen === "1" ? "启用" : "禁用"
        })
        this.total = res.total;
        this.addAvail = false;
      }catch (e) {
        this.$message.error(e)
        this.addAvail = true;
      }finally {
        this.lastTableLoading = false
      }
    },
    // 获取公开范围
    async getScope(){
      const res = await getScopeList()
      this.scopeList = res.data
    },
    // 获取过滤条件
    async loadFilterConditions(){
      const res = await getFilterConditions()
      this.filter2 = res.data
    },
    async loadFConditions(){
      const res = await getFConditions()
      this.filter4 = res.data
    },
    // 获取字段查询
    async getFieldQuery(){
      const res = await getField({fieldName:'',recNo:this.queryParams.sortMainTableNo})
      this.filter1 = res.rows
      this.filter1.forEach(item => {
        item.dictLabel = item.fieldAlias
        item.dictValue = item.fieldName
      })
    },
    // 获取密级
    async getSecurity(){
      const res = await getSecurityList()
      this.securityList = res.data
    },
    async getTime() {
      const res = await getTimeRange();
      this.timeRange = res.data;
    }
  }
}
