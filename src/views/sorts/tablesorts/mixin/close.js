export const close = {
  methods:{
    // 关闭非底层弹窗
    closeOpen(){
      this.open = false
      // this.queryParams.sortName = ""
    },
    // 关闭表信息弹出层
    closeTableInfo(){
      this.tableInfoOpen = false
    },
    // 关闭表关系弹出层
    closeTRelationDia(){
      this.tableRelationDialogVisible = false
    },
    // 关闭数据结构弹出层
    closeDStructure(){
      this.dataStructureDialogVisible = false
    },
    // 关闭存储设置弹出层
    closeSSettings(){
      this.storeSettingsDialogVisible = false
    },
    // 关闭档号规则弹出层
    closeNoRules(){
      this.NoRulesDialogVisible = false
    },
    // 关闭鉴定规则弹出层
    closeIdRules(){
      this.IdRulesDialogVisible = false
    }
  }
}
