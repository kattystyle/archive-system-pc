export const add = {
  methods:{
    // 新增按钮操作
    handleAdd() {
      this.findLast(this.treeData)
      // isUnderlying为0，说明是底层
      if(this.isUnderlying === "1"){
        this.reset();
        this.title = "新增分类";
        this.form.orderNo = Math.max.apply(null,this.orderNos) + 1
        if(this.nodeData.length === 0){
          this.nodeData = "档案分类"
        }
        this.form.sortPrantName = this.nodeData
        this.open = true;
        //父级编号
        this.form.sortPrantNo=this.queryParams.sortPrantNo;
      }else if(this.nodeDataNo === '001'){
        for (const key in this.tableInfoForm) {
          this.tableInfoForm[key] = ""
        }
        this.tableInfoForm.tableType = "0"
        this.tableInfoTitle = `新增${this.nodeData}`
        this.tableInfoOpen = true
      }else if(this.nodeDataNo === '002'){
        this.tableRelationTitle = `新增${this.nodeData}`
        this.tableRelationForm.update = '新增'
        this.tableRelationForm.info = []
        this.tableRelationDialogVisible = true
      }else if(this.nodeDataNo === '003'){
        this.dataStructureTitle = `新增${this.nodeData}`
        this.dataStructureDialogVisible = true
      }else if(this.nodeDataNo === '004'){
        this.storeSettingsTitle = `新增${this.nodeData}`
        this.storeSettingsDialogVisible = true
      }else if(this.nodeDataNo === '005'){
        this.NoRulesTitle = `新增${this.nodeData}`
        this.NoRulesDialogVisible = true
      }else if(this.nodeDataNo === '006'){
        this.IdRulesTitle = `新增${this.nodeData}`
        this.IdRulesForm.cate = this.queryParams.sortName
        if(this.IdRuleList.length >= 1){
          return this.$message.error('鉴定规则只允许有一个')
        }
        this.IdRulesDialogVisible = true
      }else{
        this.$message.info('请选择分类')
      }
    },
  }
}
