import {
  delArNoPolicy, delIdRules,
  delSaveSettings,
  delSort,
  delStructDetail,
  delTableRelation,
  lastDelSort
} from '@/api/system/sort'

export const del = {
  methods:{
    // 删除按钮操作
    handleDelete(row) {
      if (this.queryParams.isLast === "1") {
        const recNos = row ? row.recNo : this.ids
        const sortNames = row ? row.sortName : this.sNames
        this.$confirm('是否确认删除分类管理名称为"' + sortNames + '"的数据项?', "警告", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then((res) => {
          this.handleDelSort(recNos)
        }).catch((err) => {
          this.$message.error('取消删除')
        })
      } else {

        this.lastHandleDelete(row)
      }
    },
    async handleDelSort(recNos){
      const res = await delSort(recNos);
      if (res.code === 200) {
        await this.getList();
        await this.msgSuccess("删除成功");
        await this.getTree()
      } else {
        this.$message.error(res.msg)
      }
    },
    lastHandleDelete(row){
      const recNos = row ? row.recNo : this.ids
      const sortNames = row ? row.sortName : this.sNames
      switch(this.nodeDataNo){
        case "001":
          this.delTableInfo(row)
          break
        case "002":
          this.delTableRelation(row)
          break
        case "003":
          this.delStructData(row)
          break
        case "004":
          this.delSave(row)
          break
        case "005":
          this.delNoRules(row)
          break
        case "006":
          this.delIdRule(row)
          break
      }
    },
    // 删除表信息
    async delTableInfo(row){
      const tips = row ? '是否确认删除表信息名称为"' + row.tableAlias + '"的数据项?' : '确认删除吗？'
      const recNos = row ? row.recNo : this.ids
      this.$confirm(tips, "警告", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(async () => {
        await lastDelSort(recNos);
        await this.getList();
        await this.msgSuccess("删除成功");
        await this.getTree()
      }).catch(err => {
        this.$message('取消删除');
      })
    },
    // 删除表关系
    delTableRelation(row){
      const tips = row ? '是否确认删除表关系名称为"' + row.relationName + '"的数据项?' : '确认删除吗？'
      const recNos = row ? row.recNo : this.ids
      this.$confirm(tips, "警告", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(async () => {
        const res = await delTableRelation(recNos);
        if(res.code === 200){
          this.$message.success(res.msg)
          await this.getTableRelation()
        }
      }).catch(err => {
        this.$message('取消删除');
      })
    },
    // 删除数据结构
    delStructData(row){
      const tips = row ? '是否确认删除结构名称为"' + row.structName + '"的数据项?' : '确认删除吗？'
      const recNos = row ? row.recNo : this.ids
      this.$confirm(tips, "警告", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(async () => {
        const res = await delStructDetail(recNos);
        if(res.code === 200){
          this.$message.success(res.msg)
          await this.getStruct()
        }
      }).catch(err => {
        this.$message('取消删除');
      })
    },
    // 删除存储设置
    delSave(row){
      const tips = row ? '是否确认删除策略名称为"' + row.policyName + '"的数据项?' : '确认删除吗？'
      const recNos = row ? row.recNo : this.ids
      this.$confirm(tips, "警告", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(async () => {
        const res = await delSaveSettings(recNos);
        if(res.code === 200){
          this.$message.success(res.msg)
          await this.getSave()
        }
      }).catch(err => {
        this.$message('取消删除');
      })
    },
    // 删除档号规则
    delNoRules(row){
      const tips = row ? '是否确认删除档号规则名称为"' + row.policyName + '"的数据项?' : '确认删除吗？'
      const recNos = row ? row.recNo : this.ids
      this.$confirm(tips, "警告", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(async () => {
        const res = await delArNoPolicy(recNos);
        if(res.code === 200){
          this.$message.success(res.msg)
          await this.getArNoPolicy()
        }
      }).catch(err => {
        this.$message('取消删除');
      })
    },
    // 删除鉴定规则
    delIdRule(row){
      const tips = row ? '是否确认删除鉴定规则名称为"' + row.authenticateName + '"的数据项?' : '确认删除吗？'
      const recNos = row ? row.recNo : this.ids
      this.$confirm(tips, "警告", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(async () => {
        const res = await delIdRules(recNos);
        if(res.code === 200){
          this.$message.success(res.msg)
          await this.getIdRulesList()
        }
      }).catch(err => {
        this.$message('取消删除');
      })
    },
  }
}
