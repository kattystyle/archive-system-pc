import request from '@/utils/request'

export function addFileStorage(data) {
    return request.post("/system/storehouse/add", data);
}

export function getStorageList(params) {
    return request.get("/system/storehouse/list", {params: {
        parentNo:params
    }});
}

export function getHouseDetail(params) {
    return request.get("/system/storehouse/getInfo/"+params);
}

export function editHouse(data) {
    return request.post("/system/storehouse/edit", data);
}

export function delHouse(params) {
    return request.get("/system/storehouse/remove");
}

export function getStorageTree() {
    return request.get("/system/storehouse/typestree");
}

export function removeHouse(params) {
    return request.get("/system/storehouse/remove/"+params);
}