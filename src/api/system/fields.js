import request from '@/utils/request'

// 查询档案字段设置列表
export function listFields(query) {
  return request({
    url: '/system/fields/list',
    method: 'get',
    params: query
  })
}

// 查询档案字段设置详细
export function getFields(recNo) {
  return request({
    url: '/system/fields/get/' + recNo,
    method: 'get'
  })
}

// 新增档案字段设置
export function addFields(data) {
  return request({
    url: '/system/fields',
    method: 'post',
    data: data
  })
}

// 修改档案字段设置
export function updateFields(data) {
  return request({
    url: '/system/fields/edit',
    method: 'post',
    data: data
  })
}

// 删除档案字段设置
export function delFields(recNo) {
  return request({
    url: '/system/fields/' + recNo,
    method: 'get'
  })
}

// 导出档案字段设置
export function exportFields(query) {
  return request({
    url: '/system/fields/export',
    method: 'get',
    params: query
  })
}

export function acquireDoc(tableNo) {
  return request.get("/system/fields/get", {params: {tableNo: tableNo}});
}