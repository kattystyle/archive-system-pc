import request from '@/utils/request'

export function getImgTree(params){
  return request({
    method:'get',
    url:'/system/query/picture',
    params
  })
}
