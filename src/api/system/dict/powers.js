import request from '@/utils/request'

// 查询职权信息列表
export function listPowers(query) {
  return request({
    url: '/system/powers/list',
    method: 'get',
    params: query
  })
}

// 查询职权信息详细
export function getPowers(recNo) {
  return request({
    url: '/system/powers/' + recNo,
    method: 'get'
  })
}

// 新增职权信息
export function addPowers(data) {
  return request({
    url: '/system/powers',
    method: 'post',
    data: data
  })
}

// 修改职权信息
export function updatePowers(data) {
  return request({
    url: '/system/powers',
    method: 'put',
    data: data
  })
}

// 删除职权信息
export function delPowers(recNo) {
  return request({
    url: '/system/powers/' + recNo,
    method: 'delete'
  })
}

// 导出职权信息
export function exportPowers(query) {
  return request({
    url: '/system/powers/export',
    method: 'get',
    params: query
  })
}