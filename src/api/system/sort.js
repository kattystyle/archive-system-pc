import request from '@/utils/request'

// 查询分类管理列表
export function listSort(query) {
  return request({
    url: '/system/sort/querylist',
    method: 'get',
    params: query
  })
}

// 查询分类管理详细
export function getSort(recNo) {
  return request({
    url: '/system/sort/' + recNo,
    method: 'get'
  })
}

// 新增分类管理
export function addSort(data) {
  return request({
    url: '/system/sort',
    method: 'post',
    data: data
  })
}

// 修改分类管理
export function updateSort(data) {
  return request({
    url: '/system/sort/editList',
    method: 'post',
    data: data
  })
}

// 非底层删除分类管理
export function delSort(recNo) {
  return request({
    url: '/system/sort/delete/' + recNo,
    method: 'get'
  })
}
// 底层删除
export function lastDelSort(recNo) {
  return request({
    url: '/system/table/' + recNo,
    method: 'get'
  })
}
// 导出分类管理
export function exportSort(query) {
  return request({
    url: '/system/sort/export',
    method: 'get',
    params: query
  })
}

//查询左侧树形数据
export function getTreeData() {
  return request({
    url:'/system/sort/lists',
    method:'get'
  })
}
// 查询表名称
export function getTableName() {
  return request({
    url:'/system/table/nonTemporary',
    method:'get'
  })
}
// 底层菜单修改
export function getLastTableEdit(recNo) {
  return request({
    url:'/system/table/get/' + recNo,
    method:'get'
  })
}
// 底层菜单提交修改
export function submitLastTableEdit(data) {
  return request({
    url:'/system/table/edit',
    method:'post',
    data
  })
}
// 底层菜单提交新增
export function submitLastTableAdd(data) {
  return request({
    url:'/system/table',
    method:'post',
    data
  })
}
// 查询表字段列表数据
export function getTableFieldList(params) {
  return request({
    url:'/system/fields/get',
    method:'get',
    params
  })
}
// 新增表字段
export function addTableField(data) {
  return request({
    url:'/system/fields',
    method:'post',
    data
  })
}
// 修改表字段获取详情
export function getTableFieldDetail(recNo) {
  return request({
    url:'/system/fields/select/'+ recNo,
    method:'get',
  })
}
// 提交修改表字段
export function editTableFieldDetail(data) {
  return request({
    url:'/system/fields/edit',
    method:'post',
    data
  })
}
// 删除表字段
export function delTableField(recNos) {
  return request({
    url:'/system/fields/'+ recNos,
    method:'get',
  })
}
// 获取字典类型
export function getDictType() {
  return request({
    url:'/system/dict/type/optionselect',
    method:'get',
  })
}
// 点击搜索获取列表
export function getSearchList(params) {
  return request({
    url:'/system/sort/list',
    method:'get',
    params
  })
}
// 表关系
export function getTableRelationList(recNo) {
  return request({
    url:'/system/tablerelation/list',
    method:'get',
    params: {
      sortNo: recNo
    }
  })
}
// 表关系表名下拉列表
export function getTableRelationNameList(params) {
  return request({
    url:'/system/sort/querylist',
    method:'get',
    params
  })
}
// 表关系表字段下拉列表
export function getTableRelationFieldList(params) {
  return request({
    url:'/system/fields/get',
    method:'get',
    params
  })
}
// 表关系新增
export function addTableRelation(data) {
  return request({
    url:'/system/tablerelation',
    method:'post',
    data
  })
}
// 获取表关系详细信息
export function getTableRelationDInfo(recNo) {
  return request({
    url:'/system/tablerelation/' + recNo,
    method:'get',
  })
}
// 表关系修改
export function updateTableRelation(data) {
  return request({
    url:'/system/tablerelation/edit',
    method:'post',
    data
  })
}
// 表关系删除
export function delTableRelation(recNos) {
  return request({
    url:'/system/tablerelation/remove/' + recNos,
    method:'get',
  })
}
// 数据结构
export function getStructList(recNo) {
  return request({
    url:'/system/struct/list',
    method:'get',
    params: {
      sortNo: recNo
    }
  })
}
// 新增数据结构
export function addStructData(data) {
  return request({
    url:'/system/struct',
    method:'post',
    data
  })
}
// 获取数据结构详细信息
export function getStructDetail(recNo) {
  return request({
    url:'/system/struct/' + recNo,
    method:'get',
  })
}
// 修改数据结构
export function updateStructDetail(data) {
  return request({
    url:'/system/struct/edit',
    method:'post',
    data
  })
}
// 删除数据结构
export function delStructDetail(recNos) {
  return request({
    url:'/system/struct/remove/'+recNos,
    method:'get'
  })
}
//存储设置
export function getSaveList(recNo) {
  return request({
    url:'/system/policy/list',
    method:'get',
    params: {
      sortNo: recNo
    }
  })
}
// 新增存储设置
export function addSaveSettings(data) {
  return request({
    url:'/system/policy',
    method:'post',
    data
  })
}
//获取存储设置详细信息
export function getSaveSettingsDetail(recNo) {
  return request({
    url:'/system/policy/'+recNo,
    method:'get',
  })
}
// 获取存储设置字典
export function getSaveSettingsDict(params) {
  return request({
    url:'/system/fields/get',
    method:'get',
    params
  })
}
// 修改存储设置
export function updateSaveSettings(data) {
  return request({
    url:'/system/policy/edit',
    method:'post',
    data
  })
}
// 删除存储设置
export function delSaveSettings(recNos) {
  return request({
    url:'/system/policy/remove/'+recNos,
    method:'get',
  })
}
//档号规则
export function getArNoPolicyList(recNo) {
  return request({
    url:'/system/nopolicy/list',
    method:'get',
    params: {
      sortNo: recNo
    }
  })
}
// 获取档号规则字典
export function getArNoPolicyDict(params) {
  return request({
    url:'/system/dict/data/type/ar_no_rules',
    method:'get',
    params
  })
}
// 新增档号规则
export function addArNoPolicy(data) {
  return request({
    url:'/system/nopolicy',
    method:'post',
    data
  })
}
// 获取档号规则详细信息
export function updateArNoPolicy(recNo) {
  return request({
    url:'/system/nopolicy/'+recNo,
    method:'GET',
  })
}
// 修改档号规则
export function submitUpdateArNoPolicy(data) {
  return request({
    url:'/system/nopolicy/edit',
    method:'post',
    data
  })
}
// 删除档号规则
export function delArNoPolicy(recNos) {
  return request({
    url:'/system/nopolicy/remove/'+ recNos,
    method:'get',
  })
}
// 鉴定规则
export function getIdRules(recNo) {
  return request({
    url:'/system/authenticate/list',
    method:'get',
    params: {
      sortNo: recNo
    }
  })
}
// 新增鉴定规则
export function addIdRules(data) {
  return request({
    url:'/system/authenticate',
    method:'post',
    data
  })
}
// 获取鉴定规则详细信息
export function getIdRulesDetail(recNo) {
  return request({
    url:'/system/authenticate/'+ recNo,
    method:'get',
  })
}
// 修改鉴定规则
export function updateIdRules(data) {
  return request({
    url:'/system/authenticate/edit',
    method:'post',
    data
  })
}
// 删除鉴定规则
export function delIdRules(recNos) {
  return request({
    url:'/system/authenticate/remove/' + recNos,
    method:'get',
  })
}
// 公开范围字典接口
export function getScopeList(params) {
  return request({
    url:'/system/dict/data/type/ar_open_scope',
    method:'get',
    params
  })
}
// 密级字典接口
export function getSecurityList(params) {
  return request({
    url:'/system/dict/data/type/ar_security_level',
    method:'get',
    params
  })
}
// 过滤条件1
export function getFilterConditions(params) {
  return request({
    url:'/system/dict/data/type/ar_filter',
    method:'get',
    params
  })
}
// 过滤条件2
export function getFConditions(params) {
  return request({
    url:'/system/dict/data/type/ar_filter_ao',
    method:'get',
    params
  })
}
// 获取字段查询
export function getField(params) {
  return request({
    url:'/system/fields/get',
    method:'get',
    params
  })
}

export function getTimeRange() {
  return request.get("/system/dict/data/type/ar_keep_time");
}