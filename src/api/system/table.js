import request from '@/utils/request'

// 查询档案设置列表
export function listTable(query) {
  return request({
    url: '/system/table/list',
    method: 'get',
    params: query
  })
}

// 查询档案设置详细
export function getTable(recNo) {
  return request({
    url: '/system/table/get/' + recNo,
    method: 'get'
  })
}

// 新增档案设置
export function addTable(data) {
  return request({
    url: '/system/table',
    method: 'post',
    data: data
  })
}

// 修改档案设置
export function updateTable(data) {
  return request({
    url: '/system/table/edit',
    method: 'post',
    data: data
  })
}

// 删除档案设置
export function delTable(recNo) {
  return request({
    url: '/system/table/' + recNo,
    method: 'get'
  })
}

// 导出档案设置
export function exportTable(query) {
  return request({
    url: '/system/table/export',
    method: 'get',
    params: query
  })
}
