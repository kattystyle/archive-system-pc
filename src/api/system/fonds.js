import request from '@/utils/request'

// 查询全宗信息列表
export function listFonds(query) {
  return request({
    url: '/system/fonds/list',
    method: 'get',
    params: query
  })
}

// 查询全宗信息详细
export function getFonds(query) {
  return request({
    url: '/system/fonds/selectbyid',
    method: 'get',
	params: query
  })
}

// 新增全宗信息
export function addFonds(data) {
  return request({
    url: '/system/fonds',
    method: 'post',
    data: data
  })
}

// 修改全宗信息
export function updateFonds(data) {
  return request({
    url: '/system/fonds/edit',
    method: 'post',
    data: data
  })
}

// 删除全宗信息
export function delFonds(recNo) {
  return request({
    url: '/system/fonds/' + recNo,
    method: 'get'
  })
}

// 删除全宗信息
export function delFondstwo(query) {
  return request({
    url: '/system/fonds/removeid',
    method: 'get',
	params: query
  })
}

// 导出全宗信息
export function exportFonds(query) {
  return request({
    url: '/system/fonds/export',
    method: 'get',
    params: query
  })
}