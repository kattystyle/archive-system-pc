import request from '@/utils/request'

// 查询字典信息列表
export function listInfo(query) {
  return request({
    url: '/system/info/list',
    method: 'get',
    params: query
  })
}

// 查询字典信息详细
export function getInfo(dictInfoId) {
  return request({
    url: '/system/info/' + dictInfoId,
    method: 'get'
  })
}

// 新增字典信息
export function addInfo(data) {
  return request({
    url: '/system/info',
    method: 'post',
    data: data
  })
}

// 修改字典信息
export function updateInfo(data) {
  return request({
    url: '/system/info',
    method: 'put',
    data: data
  })
}

// 删除字典信息
export function delInfo(dictInfoId) {
  return request({
    url: '/system/info/' + dictInfoId,
    method: 'delete'
  })
}

// 导出字典信息
export function exportInfo(query) {
  return request({
    url: '/system/info/export',
    method: 'get',
    params: query
  })
}