import request from '@/utils/request'

// 查询借阅管理列表
export function listInfo(query) {
  return request({
    url: '/borrowRegister/info/list',
    method: 'get',
    params: query
  })
}

// 查询借阅管理详细
export function getInfo(recNo) {
  return request({
    url: '/borrowRegister/info/selectbyid/' + recNo,
    method: 'get'
  })
}

// 新增借阅管理
export function addInfo(data) {
  return request({
    url: '/borrowRegister/info',
    method: 'post',
    data: data
  })
}

// 修改借阅管理
export function updateInfo(data) {
  return request({
    url: '/borrowRegister/info/edit',
    method: 'post',
    data: data
  })
}

// 删除借阅管理
export function delInfo(recNo) {
  return request({
    url: '/borrowRegister/info/' + [recNo],
    method: 'get'
  })
}

// 导出借阅管理
export function exportInfo(query) {
  return request({
    url: '/borrowRegister/info/export',
    method: 'get',
    params: query
  })
}

export function excludeDoc(arBorrowDetail) {
  return request.post("/web/detail/edit", arBorrowDetail);
}