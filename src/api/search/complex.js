import request from '@/utils/request'

export function search(sortMainTableNo, searchValue) {
    return request.get("/system/query", {params:{sortMainTableNo, searchValue}})
}

export function getClass() {
    return request.get("/system/sort/lists");
}

export function searchByRow(recNo) {
    return request.get("/system/query/tree", {params: {recNo}});
}

export function searchByTree(id, tableName) {
    return request.get("/system/query/get", {params: {id, tableName}});
}