import request from '@/utils/request'

// 获取树形数据
export function getTreeList (params){
  return request({
    url:'/system/sort/lists',
    method:'get',
    params
  })
}
// 过滤条件1
export function getFilterLabel(params) {
  return request({
    url:'/system/fields/get',
    method:'get',
    params
  })
}

// 过滤条件2
export function getBiggerSmaller(params) {
  return request({
    url:'/system/dict/data/type/ar_filter',
    method:'get',
    params
  })
}

// 过滤条件4
export function getAndOr(params) {
  return request({
    url:'/system/dict/data/type/ar_filter_ao',
    method:'get',
    params
  })
}
// 获取列表数据
export function getQueryList(params) {
  return request({
    url:'/system/query/list',
    method:'get',
    params
  })
}
// 获取查看弹窗树形数据
export function getViewTreeList(params) {
  return request({
    url:'/system/query/tree',
    method:'get',
    params
  })
}

// 获取查看弹窗表单回显详情
export function getViewTreeForm(params) {
  return request({
    url:'/system/query/get',
    method:'get',
    params
  })
}
